<?php // 12.10.18 порядок в структуре

header('Content-Type: text/html; charset=utf-8');

$c = array('dev_id'  => "0",
           'dev_key' => "CometQLPasswordOOOOODdf34gb5434dfbtd4f55ybd4yf45b4dbf");

$aActualCountries = array(
  "AD","AF","AL","AR","AT","AU","AZ","BA","BE","BG","BY","CH","CN","CZ","DE","DK","EE","ES","FI","FR","GB",
  "GE","GR","HR","HU","IE","IL","IN","IQ","IR","IS","IT","JO","KG","KZ","LI","LT","LU","LV","MC","MD","ME",
  "MK","MN","NL","NO","PK","PL","PS","PT","RO","RS","RU","SA","SE","SI","SK","SY","TH","TJ","TM","TR","UA",
  "UZ","VN","ZZ");

//============================================================================================
function del_cargo($link) {

    mysqli_query($link, 'DELETE FROM pipes_messages WHERE name = "cargo";'); 
    bye($link);

}

//============================================================================================

function del_transport($link) {

    mysqli_query($link, 'DELETE FROM pipes_messages WHERE name = "transport";');
    bye($link);

}

//============================================================================================

function del_all($link) {

    del_cargo($link);
    del_transport($link);

}

//============================================================================================
function test_cargo($link) {

   $url = "https://lardi-trans.ru/gruz/?foi=&filter_marker=new&countryfrom=RU&countryto=RU&cityFrom=&cityIdFrom=&cityTo=&cityIdTo=&dateFrom=&dateTo=&bt_chbs_slc=&strictBodyTypes=on&mass=&mass2=&value=&value2=&gabDl=&gabSh=&gabV=&zagruzFilterId=&adr=-1&showType=all&sortByCountriesFirst=on&startSearch=%D0%A1%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C+%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D1%83&notFirstLoad=done";

    // делаем парсинг страницы 47 записей
    $oPage = new oParsingPage($url);
    $oPage->scGetTableLines();
    $oPage->scPutPrintR();

    foreach($oPage->aTableLines as $aLines) {
        send($link, $aLines, 'cargo');
    }
    bye($link);

}

//============================================================================================
function test_transport($link) {

    $url = "https://lardi-trans.ru/trans/?foi=&filter_marker=new&countryfrom=RU&countryto=RU&cityFrom=&cityIdFrom=&cityTo=&cityIdTo=&dateFrom=&dateTo=&bt_chbs_slc=&mass=&mass2=&value=&value2=&gabDl=&gabSh=&gabV=&zagruzFilterId=&adr=-1&sortByCountriesFirst=on&startSearch=%D0%A1%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C+%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D1%83&notFirstLoad=done";

    // делаем парсинг страницы 47 записей
    $oPage = new oParsingPage($url, 't');
    $oPage->scGetTableLines();
    $oPage->scPutPrintR();

    foreach($oPage->aTableLines as $aLines) {
        send($link, $aLines, 'transport');
    }
    bye($link);

}

// ===SC=== Выборка очереди грузов или транспорта с целью определения
// самого большого времени заявки
function scGetQueueMaxTime($cQtype = 'cargo') {

  // читаем очередь
  $aQueue = rows("select * from pipes_messages where name like '{$cQtype}'");
  $iMaxTime = 0;
  $cMaxCode = '';
  $cCurrMD5 = '';

  foreach ($aQueue as $aLine) {
    $aItem = json_decode(base64_decode($aLine['message']));
    // искаем в текущей записи уникальный код
    foreach ($aItem as $cKey => $cVal) {
      if ($cKey == 'uq_md5') { $cCurrMD5 = $cVal; break; }
    }
    // искаем в текущей записи метку времени
    foreach ($aItem as $cKey => $cVal) {
      if ($cKey == 'uq_code') {
        // если метка времени текущей записи больше: запоминаем ее и MD5-код этой записи
        if ($cVal >= $iMaxTime) {
          $iMaxTime = $cVal;
          $cMaxCode = $cCurrMD5;
          echo $iMaxTime . ' (' . $cMaxCode . ') >> ';
          break;
        }
      }
    }
  }
  echo '<br/> Stop at scGetQueueMaxTime = ' . $iMaxTime  . ' (' . $cMaxCode . ')<br/><br/>';
  $aResult['time'] = $iMaxTime;
  $aResult['code'] = $cMaxCode;
  return $aResult;

}

//============================================================================================

function parse_cargo($link) {

  global $aActualCountries;

  echo 'Cargo<br/><br/>';

  $url = "https://lardi-trans.ru/gruz/?foi=&filter_marker=new&countryfrom=RU&countryto=RU&cityFrom=&cityIdFrom=&cityTo=&cityIdTo=&dateFrom=&dateTo=&bt_chbs_slc=&strictBodyTypes=on&mass=&mass2=&value=&value2=&gabDl=&gabSh=&gabV=&zagruzFilterId=&adr=-1&showType=all&sortByCountriesFirst=on&startSearch=%D0%A1%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C+%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D1%83&notFirstLoad=done";

  // делаем парсинг страницы, эмулируя список заявок в очереди
  $oPage = new oParsingPage($url);
  $oPage->scGetTableLines('Y');

  if (count($oPage->aTableLines) > 0) {
    // выборка не пустая
    $iCount = 0;
    $aResult = scGetQueueMaxTime('cargo');

    foreach($oPage->aTableLines as $aLine) {
      if ($aLine['uq_code'] >= $aResult['time']) {
        // время текущей заявки больше или равно последней в очереди,
        // если их уникальные коды не совпадают: добавляем ее в очередь
        if ($aResult['code'] != $aLine['uq_md5']) {
        echo 'append >> ' . $aLine['from'][0]['city'] . ' - ' . $aLine['to'][0]['city'] . '---<br/>';
          send($link, $aLine, 'cargo');
        }
      } else {
//        echo '---УЖЕ ЕСТЬ >>' . $aLine['uq_code'] . ': ' . $aLine['from'][0]['city'] . ' - ' . $aLine['to'][0]['city'] .  '---<br/>';
      }
    }

    bye($link);
  }

}

//============================================================================================
function parse_transport($link) {

  global $aActualCountries;

  echo 'Transport<br/><br/>';

  $url = "https://lardi-trans.ru/trans/?foi=&filter_marker=new&countryfrom=RU&countryto=RU&cityFrom=&cityIdFrom=&cityTo=&cityIdTo=&dateFrom=&dateTo=&bt_chbs_slc=&mass=&mass2=&value=&value2=&gabDl=&gabSh=&gabV=&zagruzFilterId=&adr=-1&sortByCountriesFirst=on&startSearch=%D0%A1%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C+%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D1%83&notFirstLoad=done";

  // делаем парсинг страницы, эмулируя список заявок в очереди
  $oPage = new oParsingPage($url, 't');
  $oPage->scGetTableLines('Y');

  if (count($oPage->aTableLines) > 0) {
    // выборка не пустая
    $iCount = 0;
    $aResult = scGetQueueMaxTime('transport');

    foreach($oPage->aTableLines as $aLine) {
      if ($aLine['uq_code'] >= $aResult['time']) {
        // время текущей заявки больше или равно последней в очереди,
        // если их уникальные коды не совпадают: добавляем ее в очередь
        if ($aResult['code'] != $aLine['uq_md5']) {
//        echo 'append >>' . $aLine['uq_code'] . ': ' . $aLine['from'][0]['city'] . ' - ' . $aLine['to'][0]['city'] . '---<br/>';
          send($link, $aLine, 'transport');
        }
      } else {
//        echo '---УЖЕ ЕСТЬ >>' . $aLine['uq_code'] . ': ' . $aLine['from'][0]['city'] . ' - ' . $aLine['to'][0]['city'] .  '---<br/>';
      }
    }

    bye($link);
  }

}

//============================================================================================
function parse_all($link) {
  
}
//======================================================================================================================
function show_queue_cargo() {
    $res = rows('select * from pipes_messages where name like "cargo"');
    foreach($res as $x=>$r) {
        foreach ($r as $k=>$v) {
            if($k == 'message')  {$res[$x]['message'] = json_decode(base64_decode($v));}
        }
    }
    echo "<pre>"; print_r($res); echo "</pre>";
}
//======================================================================================================================
function show_queue_transport() {
    $res = rows('select * from pipes_messages where name like "transport"');
    foreach($res as $x=>$r) {
        foreach ($r as $k=>$v) {
            if($k == 'message')  {$res[$x]['message'] = json_decode(base64_decode($v));}
        }
    }
    echo "<pre>"; print_r($res); echo "</pre>";
}
//============================================================================================
function dbconnect($c)
{
  return mysqli_connect("85.143.174.149", $c['dev_id'], $c['dev_key'], "CometQL_v1", 3307);
}

function q($query) {

    if (!$GLOBALS['dbid']) dbconnect();
    if (!$result = mysqli_query($GLOBALS['dbid'], $query))
    {
        dberror(mysqli_error($GLOBALS['dbid']), mysqli_errno($GLOBALS['dbid']));
        $result = false;
    }
    dbnum();
    return $result;
}

function dbnum($show=false) {
    static $qnum=0;
    if (false !== $show) return $qnum;
    $qnum++;
}

function numrows($q) {
    return mysqli_num_rows($q);
}

function fetch($q) {
    return mysqli_fetch_assoc($q);
}

function row($query, $mode='assoc') {

    $row = array();
    if ($q = q($query)) {
        $F = 'mysqli_fetch_'.$mode;
        $row = $F($q);
        mysqli_free_result($q);
    }
    return $row;

}

function rows($query, $setkey=false, $group=false, $subkeys=false) {

    $rows = array();
    if ($q = q($query)) {
        if (false !== $setkey) {
            if (false !== $group) {
                if (false !== $subkeys)
                    while ($row = mysqli_fetch_assoc($q)) $rows[$row[$setkey]][$row[$subkeys]] = $row;
                else
                    while ($row = mysqli_fetch_assoc($q)) $rows[$row[$setkey]][] = $row;
            } else
                while ($row = mysqli_fetch_assoc($q)) $rows[$row[$setkey]] = $row;
        } else
            while ($row = mysqli_fetch_assoc($q))
                $rows[] = $row;
        mysqli_free_result($q);
    }
    return $rows;
}

//============================================================================================

function send($link, $cLines, $chanel)
{
    $result = mysqli_query($link, 'INSERT INTO pipes_messages (name, event, message)VALUES("'.$chanel.'", "event_name", "'.base64_encode(json_encode($cLines)).'")');
    if($result != 11)
    {
        return false;
    }
    return true;
}
//============================================================================================

function bye($link)
{
  mysqli_close($link);
  exit();
}

//============================================================================================
$link = dbconnect($c);
$GLOBALS['dbid'] = $link;

//$cCode = (isset($_GET['action'])) ? $_GET['action'] : 'parse_transport';

if($link){
  switch ($_GET['action']) {
      case 'del_cargo':             del_cargo($link);               break;
      case 'del_transport':         del_transport($link);           break;
      case 'del_all':               del_all($link);                 break;
      case 'test_cargo':            test_cargo($link);              break;
      case 'test_transport':        test_transport($link);          break;
      case 'parse_all':             parse_all($link);               break;
      case 'parse_cargo':           parse_cargo($link);             break;
      case 'parse_transport':       parse_transport($link);         break;
      case 'show_queue_cargo':      show_queue_cargo($link);        break;
      case 'show_queue_transport':  show_queue_transport($link);    break;

      default:                      parse_all($link);               break;
  }
}

//===SC=== СОБСТВЕННО ПАРСИНГ СТРАНИЦ =============================================================

class oParsingPage {

  public  $cMode    = 'g';        // это режим работы: 'g' - грузы; 't' - транспорт
  private $cSource  = '';         // это исходная страница, подлежащая парсингу
  public  $aTableHead = array();  // это шапка таблицы, если есть выборка - МАССИВ ИМЕН КОЛОНОК
  public  $cBody    = '';         // это часть страницы, содержащая информацию - ТЕКСТ ОТ ШАПКИ ДО КОНЦА ФАЙЛА
  private $bWork    = FALSE;      // TRUE - если есть тело и можно работать, как минмум, с шапкой
  private $iLastEnd = 0;          // здесь будет не 0, если шапка найдена и можно черпать строчки таблицы

  public  $aTableLines = array(); // здесь будут все строки таблицы
  private $aLines      = array(); // текущая строка в процессе формирования

  // списки уникальных кодов всех строк текущего объекта
  private $cUqCodes    = '';      // '['.uq_md5.']' ...

  // это заполняется методом scGetContents
  public $cCnt = '';   // строка содержимого $aOut['cnt']
  public $iPos = '';   // позиция, следующая, за выбранным $aOut['pos']
  public $bYes = '';   // $aOut['yes']:
                       //   - TRUE - что-то найдено (даже пустое место),
                       //   - FALSE - ничего не найдено

  // в конструктор передается текущая страница, из которой выделяется тело в котором производится парсинг.
  // если тела нет или оно фиговое, объект не создается. Додумать ...

  function __construct($cLink = '', $cMode = 'g')
  {

    // если ссылка не указана выбираем первую страницу грузов
    if (mb_strlen($cLink) < 4) {
      // если не указана ссылка выбираются грузы с первой страницы,
      // но это вариант RU - RU !!!
      $cLink = "https://lardi-trans.ru/gruz/?predl_tip=1&countryfrom=RU&countryto=RU&areafrom=0&areato=0&bt_chbs_slc=&dayfrom=&monthfrom=&yearfrom=&mass2=&value2=&startSearch=%D0%9F%D0%BE%D0%B8%D1%81%D0%BA&page=1&fi=309c1bde-7dae-400a-89cb-a7ec325de37f";
    }

    // если указана работа с транспортом - изменим режим
    if ($cMode == 't') {
      $this->cMode = 't';
    }
    // если информация не выбрана выдается 'Ошибка инициализации SoaClient или запроса'
    // если есть блок информации, то он начинается с тега '<span class="gwt-HTML">'
    // это шапка, конец которой по истечение выборки, затем можно выбирать строки таблицы
    // если шапки нет - выборки нет. Если есть ошибка - выборки тоже нет

    // попытка чтения содержимого страницы по заданной ссылке
//===SC===    $this->cSource = file_get_contents($cLink);
$ch = curl_init($cLink);
//curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
//curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
curl_setopt($ch, CURLOPT_REFERER, 'http://yandex.ru');
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_HEADER, false);
$this->cSource = curl_exec($ch);
curl_close($ch);
//===SC===

    // ключевые фразы
    $cError = 'Ошибка инициализации SoaClient или запроса';
    $cHead  = '<span class="gwt-HTML">';

    // проверка, что страница загрузилась
    $iPosError = mb_strpos ($this->cSource, $cError);
    $iPosOk = mb_strpos ($this->cSource, $cHead);

    if (is_integer($iPosError)) {
      // однозначно страница не загрузилась и никакой работы не будет
      $this->cBody = '';
    }

    if (is_integer($iPosOk)) {
      // обнаружено начало шапки таблицы: выбираем в $this->cBody все от начала шапки
      // и до конца содержимого страницы. Конец пока - не очевиден, хотя приличная выборка
      // завершается фразой 'Печать выбранных заявок'
      $this->cBody = mb_substr($this->cSource, $iPosOk);  // выбираем все что есть от начала шапки до конца
      $this->bWork = TRUE;
    }

  }

  // функция, которая проверяет наличие уникального кода в списке уникальных кодов данного объекта
  public function scCheckUqCode($cUqCode = '[x]') {

    $iPos = mb_strpos($this->cUqCodes, $cUqCode);
    return is_integer($iPos);  // TRUE - если код $cUqCode имеется в спике кодов этого объекта

  }

  // функция выборки содержимого 'cnt' из выбираемого: ...head|cnt|tail... внутри всего текста с позиции $iStart
  //                                                                       ^ 'pos'
  // результат:
  //    $this->cCnt - строка содержимого
  //    $this->iPos - позиция, следующая, за выбранным
  //    $this->bYes - TRUE - что-то найдено (даже пустое место), FALSE - ничего не найдено

  private function scGetContents($iStart = 0, $cHead = 'xHead', $cTail = 'xTail') {
    // ничего не найдено
    $this->cCnt = '';
    $this->iPos = 0;
    $this->bYes = FALSE;

    if (! $this->bWork) {
      return '';
    }

    // исходные данные не проверяются на корректность
    $iHlen = mb_strlen($cHead);
    $iTlen = mb_strlen($cTail);
    $iPos  = mb_strpos($this->cBody, $cHead, $iStart);

    // определение наличия выделенного
    if (is_integer($iPos)) {
      // обнаружено начало выбранного, приблизимся к его содержимому
      $iPos += $iHlen;
      $iEnd = mb_strpos($this->cBody, $cTail, $iPos);
      if (is_integer($iEnd)) {
        // найден конец содержимого
        $this->cCnt = mb_substr($this->cBody, $iPos, $iEnd - $iPos);

        // в содержимом может быть пусто, а могут быть теги
        $this->cCnt = trim($this->cCnt);
        $this->iPos = $iEnd + $iTlen;
        $this->bYes = TRUE;
      }
    }

   return $this->cCnt;

 }

  // Искаем шапку таблицы. Это - это обязательная операция перед выборкой строк
  public function scGetTableHead() {

    $cHead = '<span class="gwt-HTML">';
    $cTail = '</span>';

    if (($this->bWork) && (mb_strlen($this->cBody) > 124)) {
      // если есть немного символов в теле, значит есть и шапка и что-то под ней
      // поиск и выборка шапки таблицы

      $this->scGetContents(0, $cHead, $cTail);
      $iCheck   = 0;

      while (($this->bYes) and ($iCheck < 7)) {
        $this->aTableHead[] = $this->cCnt;
        $this->iLastEnd     = $this->iPos;  // запоминаем конец для начала анализа строк
        $this->scGetContents($this->iPos, $cHead, $cTail);
        $iCheck++;
      }
    }

    // если возврат будет 0, значит шапки нет, ну и таблицы тоже нет
    return $this->iLastEnd;

  }

  // Вспомогательные функции. Выборка заявки: начало 'time', 'route', 'c_from', 'c_to'
  private function scGetApplication() {

    // будем уверены, что массива текущей строки нет и процесс пошёл с самого нуля
    unset($this->aLines);
    $this->aLines = array();

    // разбиваем строку 'RU – RU</span><div>13:32' на две части: маршрут и время подачи заявки
    $aRT = explode('</span><div>', $this->cCnt);

    // время подачи заявки
    // трансформация $this->aLines['time'] ==>> $this->aLines['date'] & $this->aLines['time']
//    $this->aLines['time'] = $this->scGetFullTime($aRT[1]);

//    $cDDMMGG = new DateTime();
//    $this->aLines['unixtime'] = $cDDMMGG->getTimestamp();
    $this->aLines['DateTime'] = $this->scGetFullTime($aRT[1]);
    $aSelDT = explode('  ', $this->aLines['DateTime']);

    $this->aLines['date'] = (isset($aSelDT[0])) ? $aSelDT[0] : '';
    $this->aLines['time'] = (isset($aSelDT[1])) ? $aSelDT[1] : '';

    $this->aLines['route'] = trim($aRT[0]);

    // разбиваем маршрут на страну(страны)-откуда и страну(страны)-куда
    $aII = explode('–', $this->aLines['route']);
    $this->aLines['c_from'] = trim($aII[0]); // RU; RU,BY; RU,BA,SE; ... любые коды стран в любой последовательности
    $this->aLines['c_to']   = trim($aII[1]); // RU; RU,BY; RU,BA,SE; ... любые коды стран в любой последовательности

  }

  // Вспомогательные функции. Дата транспортировки: начало 'date':
  //                          array('from'=>'dd.mm') или array(('from'=>'dd.mm', 'to'=>'dd.mm')
  private function scGetTransportationDate() {

    $aSel = explode('–', $this->cCnt);

    $aDateFT['from'] = (isset($aSel[0])) ? $aSel[0] : '';
    $aDateFT['to']   = (isset($aSel[1])) ? $aSel[1] : '';

    // $this->aLines['date'] ==>> $this->aLines['load_date']
    $this->aLines['load_date'] = $aDateFT;

  }

  // Вспомогательные функции. Откуда/Куда: начальная или конечная точка маршрута 'from' или 'to':
  private function scGetPlaceFromTo($cFromTo = 'c_from') {

    $cFTNext = '</span><br /><span class="myPredlTownLine boldClass t_bold">';

    $cLine = str_replace('</span><span>', ' ', $this->cCnt);

    // проверка наличия второй позиции. А вдруг их будет больше двух?
    $iPosSecond = strpos($cLine, $cFTNext);
    if (is_integer($iPosSecond)) {
      $aLine = explode($cFTNext, $cLine);
      $aRegion = explode(',', $this->aLines[$cFromTo]);
    } else {
      $aLine[] = $cLine;
      $aRegion[] = $this->aLines[$cFromTo];
    }

    if ($this->cMode == 'g') {
      for ($iNo = 0; $iNo < count($aRegion); $iNo++) {
        $aFrom['country'] = (isset($aRegion[$iNo])) ? $aRegion[$iNo] : '-?-';
        if (isset($aLine[$iNo])) {
          $aSel = explode(' (', $aLine[$iNo]);
          $aFrom['city'] = (isset($aSel[0])) ? $aSel[0] : '-?-';
          $aFrom['area'] = (isset($aSel[1])) ? $aSel[1] : '-?-';
          $aFrom['area'] = str_replace(')', '', $aFrom['area']);
        }
        $aItems[] = $aFrom;
      }
    } else {
       for ($iNo = 0; $iNo < count($aLine); $iNo++) {
         $aSel = explode(' (', $aLine[$iNo]);
         $aFrom['city'] = (isset($aSel[0])) ? $aSel[0] : '-?-';
         $aFrom['area'] = (isset($aSel[1])) ? $aSel[1] : '-?-';
         $aFrom['area'] = str_replace(')', '', $aFrom['area']);
         $aPlaces[] = $aFrom;
       }
       $aItems[] = array('countries' => $aRegion, 'areas' => $aPlaces);
    }

    return $aItems;

  }

  // Выборка строк таблицы и формирование массива строк
  // Перед выборкой обязательно делается выборка шапки
  // если $cCount50 = 'Y' - значит выбираются все записи
  public function scGetTableLines($cCount50 = '') {

    if ($this->iLastEnd == 0) {
      // значит шапка таблицы не была выбрана и мы это делаем здесь
      if ($this->scGetTableHead() == 0) {
        // шапки не найдено, выборка строк невозможна. Ничего не делаем
        return FALSE;
      }
    }

    // шапка была выбрана и $this->iLastEnd указывает на начало строк таблицы
     $cWay     = '<span class="boldClass t_bold">'; // RU-RU ...
     $cWayStop = '</div>';

     // дата, транспорт и груз
     $cDT = 'class="uiPredlClickableCell predlInfo"><span>';
     $cDTStop = '</span>';

     // откуда и куда
     $cFT = '<span class="myPredlTownLine boldClass t_bold">';
     $cFTStop = '</span></td>';
     $cFTNext = '</span><br /><span class="myPredlTownLine boldClass t_bold">';

     // что везем
     $cWho = 'class="uiPredlClickableCell predlInfo"><span>';

     // стоимость, цена или расчет
     $cCost = 'predlInfo"><span class="t">';

     $this->scGetContents($this->iLastEnd, $cWay, $cWayStop);
     $iCheck = 0;

     $iCheckStart = ($cCount50 != 'Y') ? 3 : 0;
     $iSteps = ($cCount50 != 'Y') ? 49 : 55;

     // это предшествующий уникальный код
     unset($iBeforeCode);

     while (($this->bYes) and ($iCheck < $iSteps)) {

       // дата подачи заявки и страна(страны) откуда - куда
       $this->scGetApplication();

       // дата транспортировки
       $this->scGetContents($this->iPos, $cDT, $cDTStop);
       $this->scGetTransportationDate();

       // транспорт
       $this->scGetContents($this->iPos, $cDT, $cDTStop);
       $this->aLines['auto'] = $this->cCnt;

       // откуда едем. Перед преобразованием
       $this->scGetContents($this->iPos, $cFT, $cFTStop);
       $this->aLines['from'] = $this->scGetPlaceFromTo('c_from');

       // куда едем. Перед преобразованием
       $this->scGetContents($this->iPos, $cFT, $cFTStop);
       $this->aLines['to'] = $this->scGetPlaceFromTo('c_to');

       // груз
       $this->scGetContents($this->iPos, $cDT, $cDTStop);
       if ($this->cMode == 'g') {
         $this->aLines['cargo'] = $this->scGetInfoPWV($this->cCnt);
       } else {
         $this->aLines['transport'] = $this->scGetInfoPWV($this->cCnt);
       }

       // стоимость
       $this->scGetContents($this->iPos, $cCost, $cDTStop);
       $this->aLines['cost'] = $this->cCnt;

       // ==== удалим непотребное ================
//       unset($this->aLines['c_from']);
//       unset($this->aLines['c_to']);
//       unset($this->aLines['route']);

       // формирование уникального кода строки
       // *?*
       $this->aLines['uq_code'] = strtotime($this->aLines['DateTime']);
       if (isset($iBeforeCode)) {
         // сформирован следующий код, если он равен предшествующему, то увеличиваем его
         // на 1 и так далее
         if ($iBeforeCode == $this->aLines['uq_code']) {
           $this->aLines['uq_code']++;
         } else {
           $iBeforeCode = $this->aLines['uq_code'];
         }
       }

       if ($this->cMode == 'g') {
         $this->aLines['uq_md5'] = md5($this->aLines['DateTime'] . $this->aLines['uq_code']
           . $this->aLines['auto']
           . implode('', $this->aLines['load_date'])
           . implode('', $this->aLines['from'][0])
           . implode('', $this->aLines['to'][0])
           . $this->aLines['cargo']['product']
           . implode('', $this->aLines['cargo']['weight'])
           . implode('', $this->aLines['cargo']['volume'])
           . $this->aLines['cargo']['tail']
           . $this->aLines['cost']);
       } else {
         $this->aLines['uq_md5'] = md5($this->aLines['DateTime'] . $this->aLines['uq_code']
           . $this->aLines['auto']
           . implode('', $this->aLines['load_date'])
           . implode('', $this->aLines['from'][0]['countries'])
           . implode('', $this->aLines['from'][0]['areas'][0])
           . implode('', $this->aLines['to'][0]['countries'])
           . implode('', $this->aLines['to'][0]['areas'][0])
           . $this->aLines['transport']['product']
           . implode('', $this->aLines['transport']['weight'])
           . implode('', $this->aLines['transport']['volume'])
           . $this->aLines['transport']['tail']
           . $this->aLines['cost']);

       }

       // запись в массив очередной строки
       if ($iCheck >= $iCheckStart) {
         // запись заявки в массив происходит только в том случае, если ее уникального кода
         // еще нету в списке всех уникальных кодов
         $cNewUqMd5 = '[' . $this->aLines['uq_md5'] . ']';
         if (! is_integer(mb_strpos($this->cUqCodes, $cNewUqMd5))) {
           // такого уникального кода еще нету

           $this->aTableLines[] = $this->aLines;
           $this->cUqCodes .= $cNewUqMd5;

/*
           if ($this->cMode == 'g') {
             $this->aTableLines[] = $this->aLines;
             $this->cUqCodes .= $cNewUqMd5;
           } else {
             if (isset($this->aLines['transport']['weight'][0])) {
               $iWeight = round($this->aLines['transport']['weight'][0]);
//echo '1) iCheck = ' . $iCheck . '; ' . $iWeight . '<br/>';
               if (($iWeight > 0) && ($iWeight < 31)) {
                 $this->aTableLines[] = $this->aLines;
                 $this->cUqCodes .= $cNewUqMd5;
               } else {
//echo '2) iCheck = ' . $iCheck . 'iWeight вне допустимых пределов <br/>';
               }
             } else {
//echo '3) iCheck = ' . $iCheck . '? no iWeight <br/>';
             }
           }//
*/

         }
       }

       $this->scGetContents($this->iPos, $cWay, $cWayStop);
       $iCheck++;

     }

    // инверсия массива ??? разумно ???
    $this->aTableLines = array_reverse($this->aTableLines);

  }

  // Функция аналог <pre>print_r</pre> для целей контроля
  public function scPutPrintR() {

    if (! is_integer(mb_strpos($_SERVER['DOCUMENT_ROOT'], 'vvv.magictime.by'))) {
      echo "<pre>";
        print_r($this->aTableLines);
      echo "</pre>";
      return '';
    }

    $cPrint = '';
    foreach($this->aTableLines as $aLines) {
      if ($this->cMode == 'g') {
        $cPrint .= 'date&time=c[' . $aLines['date'] . '&nbsp;&nbsp;' . $aLines['time'] . ']&nbsp;&nbsp;'
          . 'auto=c[' . $aLines['auto']                                     . ']&nbsp;&nbsp;'
          . 'load_date=a[' . implode('; ', $aLines['load_date'])            . ']<br/>'
          . 'from=a[' . implode('; ', $aLines['from'][0])                   . ']&nbsp;&nbsp;'
          . '  to=a[' . implode('; ', $aLines['to'][0])                     . ']<br/>'
          . 'cargo.product=c[' . $aLines['cargo']['product']                . ']&nbsp;&nbsp;'
          . ' cargo.weight=a[' . implode(';', $aLines['cargo']['weight'])   . ']&nbsp;&nbsp;'
          . ' cargo.volume=a[' . implode(';', $aLines['cargo']['volume'])   . ']<br/>'
          . '   cargo.tail=c[' . $aLines['cargo']['tail']                   . ']&nbsp;&nbsp;'
          . '         cost=c[' . $aLines['cost']                            . ']<br/><br/>';
      } else {
        $cPrint .= 'date&time=c[' . $aLines['date'] . '&nbsp;&nbsp;' . $aLines['time'] . ']&nbsp;&nbsp;'
          . 'auto=c[' . $aLines['auto']                                     . ']&nbsp;&nbsp;'
          . 'load_date=a[' . implode('; ', $aLines['load_date'])            . ']<br/>'
          . 'from=a[' . implode(', ', $aLines['from'][0]['countries'])       .'; '
          .             implode(',', $aLines['from'][0]['areas'][0])          . ']&nbsp;&nbsp;<br/>'
          . '  to=a[' . implode(', ', $aLines['to'][0]['countries'])           .'; '
          .             implode(', ', $aLines['to'][0]['areas'][0])             . ']&nbsp;&nbsp;<br/>'
          . 'transport.product=c[' . $aLines['transport']['product']             . ']&nbsp;&nbsp;'
          . 'transport.weight=a[' . implode(';', $aLines['transport']['weight'])  . ']&nbsp;&nbsp;'
          . 'transport.volume=a[' . implode(';', $aLines['transport']['volume'])  . ']<br/>'
          . 'transport.tail=c[' . $aLines['transport']['tail']                    . ']&nbsp;&nbsp;'
          . '          cost=c[' . $aLines['cost']                                 . ']<br/><br/>';
      }
    }

    return $cPrint;

  }

  // распознавание даты: если нет указания на число и месяц, подразумевается текущее число текущего месяца
  //                     если распознование выполняется до 23:59 текущего числа
  // когда распознавание выполняется ночью и время оформления заявки больше текущего времени, то датой
  // оформления заявки будет предыдущий день в указанное время
  private function scGetFullTime($cSrc) {

    $aMon = array('янв'=>1, 'фев'=>2, 'март'=>3,  'апр'=>4,  'мая'=>5, 'июн'=>6, 'июл'=>7,
                  'авг'=>8, 'сент'=>9, 'окт'=>10, 'ноя'=>11, 'дек'=>12);

    $cSrc = trim($cSrc);

    if (mb_strlen($cSrc) < 6) {
      // вариант: hh:mm
      //   выясняем сегодня или вчера была оформлена заявка
      //   если текущее время больше времени заявки, то заявка была оформлена сегодня
      //   если текущее время меньше времени оформления заявки, значит она была оформлена вчера
      $dDate = GetDate();
      $cDate = str_pad($dDate['mday'], 2, '0', STR_PAD_LEFT) . '.' .
               str_pad($dDate['mon'], 2, '0', STR_PAD_LEFT) . '.' .
               $dDate['year'];
      $cDst = $cDate . '  ' . $cSrc;
    } else {
      // вариант dd месяц hh:mm - имеется указание на число и месяц оформления заявки, но никогда нет года
      //   считаем текущий год актуальным и оставляем решение проблемы конца года на потом
      //   имеем число и преобразуем название месяца, приставляя к нему текущий год
      $aSrc = explode(' ', $cSrc);
      $cDst = str_pad($aSrc[0], 2, '0', STR_PAD_LEFT) . '.' .
              str_pad($aMon[$aSrc[1]], 2, '0', STR_PAD_LEFT) . '.2018  ' . $aSrc[2];
    }

    return $cDst;

  }

  // проверка, что строка содержит только одни цифры и, возможно, одну точку
  private function scCheckNumber($cSrc, &$cDst) {

    $aI = array ('0' => TRUE, '1' => TRUE, '2' => TRUE, '3' => TRUE, '4' => TRUE,
                 '5' => TRUE, '6' => TRUE, '7' => TRUE, '8' => TRUE, '9' => TRUE);
    $fP = FALSE; // признак того, что точка была найдена

    $cSrc = trim($cSrc);

    if (mb_strlen($cSrc) > 1) {
      // в исходной строке более одного символа, начинаем работать с хвоста
      $iPos = mb_strlen($cSrc) - 1;
      $iTst = 14; // максимальное количество цифр в числе, включая точку
      while (($iPos >= 0) && ($iTst >=0 )) {
        // берем очередной символ с конца
        $cChr = mb_substr($cSrc, $iPos, 1);
        if (! @$aI[$cChr]) {
          // такой цифры нет среди доступных, проверим, что это точка и точек ранее не было найдено
          $fP = (($cChr == '.') && (! $fP));
          if (! $fP) {
            // это все же была не точка или вторая точка, значит строка не содержит правильного числа
            $cDst = '';
            return FALSE;
          }
        }
        // переходим к следующему символу
        $iPos--;
        $iTst--;
      }
      // все символы строки пройдены, значит в ней содержится реальное число
      $cDst = $cSrc;
      return TRUE;
    } else {
      // имеется только один символ или вовсе нет ничего
      if (@$aI[$cSrc]) {
        // такая цифра имеется среди доступных
        $cDst = $cSrc;
        return TRUE;
      } else {
        $cDst = '';
        return FALSE;
      }
    }
  }

  // преобразование строки в число или диапазон чисел
  private function scCheckLine($iLine, &$aResult) {

    $fResult = FALSE;
    $cDelimiter = '–';

    $iPos = strpos($iLine, $cDelimiter);
    if (is_integer($iPos)) {
      $aPara = explode($cDelimiter, $iLine);
    } else {
      $aPara[] = trim($iLine);
    }
    foreach ($aPara as $iVal) {
      $iDst = '';
      if ($this->scCheckNumber($iVal, $iDst)) {
        $aResult[] = $iDst;
        $fResult = TRUE;
      }
    }

    return $fResult;
  }

  private function scGetInfoPWV($cSrc = '') {

    $aSrc = explode(',', $cSrc);
    $aInfo['product'] = '';      // наименование продукта
    $aInfo['weight'] = array();  // вес
    $aInfo['volume'] = array();  // объем
    $aInfo['tail']   = '';       // хвостик (остаток)
//       $aInfo['alls']   = $cSrc;    // исходная строка
    $iWeight = 0;
    $iVolume = 0;
    $iNewFormat = 0;

    // в цикле перебираем все элементы в поисках веса и объема
    for ($iNo=0; $iNo<count($aSrc); $iNo++) {

      $cLine = trim($aSrc[$iNo]);
      $iLine = mb_strlen($cLine);

      // проверим наличие скобок и грохнем их
      $iPosO = mb_strpos($cLine, '(');
      if (is_integer($iPosO)) {
        // есть скобки, а в скобках что-то может быть
        $cLine = preg_replace("/[\(][т|м|0-9|\s|\+]*[\)]/", '', $cLine);
      }

      // проверяем наличие нового вариант задания веса и объема
      $iPosW = mb_strpos($cLine, 'т ');
      $iPosV = mb_strpos($cLine, 'м3');

      if ((is_integer($iPosW)) && (is_integer($iPosV))) {
        // имеем вес и объем, заданные одной строкой "9т 9м3", исключаем ситуацию:
        //   "25т (0т + 25т) 90м3 (0м3 + 90м3), ..."
        $aVal = explode('т ', $cLine);
        $cValWeight = $aVal[0];
        $iPosO = mb_strpos($aVal[1], ') ');
        // если есть скобка ')' значит есть и вторая '('; разбиваем еще раз
        if (is_integer($iPosO)) {
          $aVal2 = explode(') ', $aVal[1]);
          $iPosV = mb_strpos($aVal2[1], 'м3');
          $cValVolume = mb_substr($aVal2[1], 0, $iPosV);
        } else {
          $cValVolume = str_replace('м3', '', $aVal[1]);
        }
        if ($this->scCheckLine($cValWeight, $aInfo['weight'])) {
          $iWeight = $iNo;
          $iNewFormat = 1;
        }
        if ($this->scCheckLine($cValVolume, $aInfo['volume'])) {
          $iVolume = $iNo;
          $iNewFormat = 1;
        }
      } else {
        // попытка найти в элементе вес
        $cTail = mb_substr($cLine, $iLine - 1, 1);
        if (($iWeight < 1) && ($cTail == 'т')) {
          // если в элементе только число или диапазон (цифры, одно тире и не более двух точек), то это вес
          $iCheck = mb_substr($cLine, 0, $iLine - 1);
          if ($this->scCheckLine($iCheck, $aInfo['weight'])) {
            $iWeight = $iNo;
          }
        }

        // попытка найти в элементе объем
        $cTail = mb_substr($cLine, $iLine - 2, 2);
        if (($iVolume < 1) && ($cTail == 'м3')) {
          // если в элементе только число или диапазон (цифры, одно тире и не более двух точек), то это вес
          $iCheck = mb_substr($cLine, 0, $iLine - 2);
          if ($this->scCheckLine($iCheck, $aInfo['volume'])) {
            $iVolume = $iNo;
          }
        }
      }

      // покуда ничего не найдено лепим строку продукта
      if (($iWeight < 1) && ($iVolume < 1) && ($iNewFormat < 1)) {
        if (mb_strlen($aInfo['product']) > 0) {  $aInfo['product'] .= ', ';   }
        $aInfo['product'] .= $cLine;
      }

    }

    // уточняем строку хвоста. Берем максимальное значение $iWeight и $iVolume
    // и шлепаем до конца массива элементов
    $iMax = max($iWeight, $iVolume);
    for ($iNo=$iMax+1; $iNo<count($aSrc); $iNo++) {

      if (mb_strlen($aInfo['tail']) > 0) {  $aInfo['tail'] .= ', ';  }
      $aInfo['tail'] .= trim($aSrc[$iNo]);

    }

    return $aInfo;

  }

  private function changeLtGt($rc) {
    $rc  = str_replace("<", "&lt;", $rc);
    return str_replace(">", "&gt;", $rc);
  }
}

?>